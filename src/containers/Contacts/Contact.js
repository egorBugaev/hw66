import React from 'react'
import './Contact.css'
const Contacts = () => {
	return (
		<div className="Contacts">
			<h2>Contacts</h2>
			<ul>
				<li>Phone: +7(926) 466-78-22</li>
				<li>Email: myblog@gamil.com</li>
				<li>Skype: myblog</li>
				<li>WhatsApp: +7(926) 466-78-22</li>
			</ul>
		</div>
	)
};

export default Contacts;