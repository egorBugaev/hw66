import React, {Component, Fragment} from 'react'
import Loader from "../../components/UI/Loader/Loader";
import Modal from "../../components/UI/Modal/Modal";

const withLoadingHandler = (WrappedComponent, axios) => {
	return class extends Component {
		
		state = {
			loading: false,
			error: false
		};
		
		interceptorReq = axios.interceptors.request.use((req) => {
			this.setState({loading: true});
			return req;
		});
		
		interceptorRes = axios.interceptors.response.use((res) => {
			this.setState({loading: false});
			return res;
		}, (error) => {
			this.setState({loading: false, error});
		});
		
		componentWillUnmount() {
			axios.interceptors.response.eject(this.interceptorRes);
			axios.interceptors.request.eject(this.interceptorReq);
		}
		
		errorDismissed = () => {
			this.setState({error: false});
		};
		
		render() {
			return (
				<Fragment>
					<Modal
						closed={this.errorDismissed}
						show={this.state.error}>
						Что то пошло не так!
					</Modal>
					<Loader loading={this.state.loading}/>
					<WrappedComponent {...this.props}/>
				</Fragment>
			)
		}
	}
};

export default withLoadingHandler;