import React from 'react'
import Post from "./Post/Post";
import ErrorBoundary from "../UI/ErrorBoundary/ErrorBoundary";

const Posts = props => {
	return (
		<ul>
			{
				props.posts.map(post => {
					return (
                        <ErrorBoundary key={post.id}>
                            <Post
                                date={post.date}
                                title={post.title}
                                text={post.text}
                                readMore={() => props.readMore(post.id)}
                            />
                        </ErrorBoundary>
					)
				})
			}
		</ul>
	)
};

export default Posts;